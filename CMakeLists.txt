cmake_minimum_required(VERSION 3.6)

project(EventSystem3)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

set(TEST_COUNTER 1)
function(buildTest targetName)
    message(STATUS "Generating Test ${TEST_COUNTER}")

    add_executable(${targetName} tests/${targetName}.cpp)
    target_link_libraries(${targetName} eventsystem)

    # Add a dummy target which depends on this target (this allows us to go back
    #  and depend on this test without knowing the name of the test).
    add_custom_target(__TEST_DUMMY${TEST_COUNTER} DEPENDS ${targetName})

    add_test(${targetName}Test ${targetName})

    # Create a dependency on the previous test to ensure that they build in the
    #  order they were declared in

    math(EXPR D "${TEST_COUNTER}-1")
    # Make sure that we don't depend on a non-existant test the first time we
    #  run this function
    if(NOT ${D} EQUAL "0")
        message(STATUS "Creating dependency of ${targetName} to __TEST_DUMMY${D}")
        add_dependencies(${targetName} __TEST_DUMMY${D})
    endif()

    # Only perform a memory check if a memory tool is present
#    if(MEMTOOL)
#        add_test(NAME ${targetName}TestMem
#                 COMMAND ${MEMTOOL} ${MEM_OPTIONS} "$<TARGET_FILE:${targetName}>")
#    else()
#        message(WARNING "No MEMTOOL found, not generating ${targetName}TestMem")
#    endif()

    # Increment the test_counter
    math(EXPR TEST_COUNTER "${TEST_COUNTER}+1")
    set(TEST_COUNTER ${TEST_COUNTER} PARENT_SCOPE)
endfunction()

# https://chromium.googlesource.com/chromium/llvm-project/libcxx/+/master/cmake/Modules/HandleLibcxxFlags.cmake
macro(remove_flags)
  foreach(var ${ARGN})
    string(REPLACE "${var}" "" CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG}")
    string(REPLACE "${var}" "" CMAKE_CXX_FLAGS_MINSIZEREL "${CMAKE_CXX_FLAGS_MINSIZEREL}")
    string(REPLACE "${var}" "" CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE}")
    string(REPLACE "${var}" "" CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO}")
    string(REPLACE "${var}" "" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
    string(REPLACE "${var}" "" CMAKE_C_FLAGS "${CMAKE_C_FLAGS}")
    string(REPLACE "${var}" "" CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS}")
    string(REPLACE "${var}" "" CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS}")
    string(REPLACE "${var}" "" CMAKE_SHARED_MODULE_FLAGS "${CMAKE_SHARED_MODULE_FLAGS}")
    remove_definitions(${var})
  endforeach()
endmacro(remove_flags)

if(${MSVC})
    remove_flags(-gline-tables-only)

    message(STATUS "USING MSVC")
    set(CMAKE_CXX_FLAGS "/std:c++17 /MP /WX /W4 /nologo /Od -fuse-ld=lld -MTd -Z7 -Wno-unknown-argument -Wno-unused-command-line-argument /EHsc"
        CACHE STRING "Flags used by the compiler during all build types." FORCE)
else()
    message(STATUS "USING CLANG")
    # set(CMAKE_CXX_COMPILER clang++)
    set(CMAKE_CXX_FLAGS "-std=c++1z -g -fvisibility-ms-compat -Wall -Wextra -Werror -Wno-unused-variable -Wno-unused-parameter -pthread"
        CACHE STRING "Flags used by the compiler during all build types." FORCE)
endif()

set(SOURCES src/EventBus.cpp src/EventBase.cpp src/EventFireError.cpp src/EventHandler.cpp)

include_directories(${CMAKE_SOURCE_DIR}/inc)

add_library(eventsystem ${SOURCES})

enable_testing()

find_program(MEMTOOL valgrind NAMES drmemory)
if(MEMTOOL)
    message(STATUS "Found memory tool `${MEMTOOL}`.")

    if(${MEMTOOL} MATCHES "valgrind")
        set(VALGRIND_SUPPRESS "${PROJECT_SOURCE_DIR}/suppress")
        set(MEM_OPTIONS --trace-children=yes --leak-check=full --quiet --error-exitcode=1 --suppressions=${VALGRIND_SUPPRESS})
    elseif(${MEMTOOL} MATCHES "drmemory")
        set(MEM_OPTIONS -quiet -exit_code_if_errors 1 -show_reachable -batch -- )
    endif()
endif()

find_program(CTEST_MEMORYCHECK_COMMAND NAMES valgrind)
include(Dart)

buildTest(link)
buildTest(busupdate)
buildTest(eventdef)
buildTest(eventfire)
buildTest(handlersubscribe)
buildTest(methodsubscribe)
buildTest(priority)
buildTest(time)
buildTest(threading)

# Build Tests manually since we'll need more fine-tuned control over how the
#  test must be set up

# Only build the graphics test if SDL and OpenGL are installed
find_package(GLEW)
find_package(OpenGL)
if(FALSE AND OPENGL_FOUND AND GLEW_FOUND)
    include_directories(SDL2_INCLUDES)
    include_directories(GLEW_INCLUDES)
    include_directories(OPENGL_gl_INCLUDES)

    add_executable(Graphics tests/graphics/graphics.cpp
                            tests/graphics/Affine.cpp
                            tests/graphics/Camera.cpp
                            tests/graphics/CubeMesh.cpp
                            tests/graphics/FrustumMesh.cpp
                            tests/graphics/Projection.cpp
                            tests/graphics/Render.cpp
                            tests/graphics/SnubDodecMesh.cpp)
    target_link_libraries(Graphics eventsystem
                                   ${CMAKE_THREAD_LIBS_INIT}
                                   SDL2
                                   ${GLEW_LIBRARIES}
                                   ${OPENGL_gl_LIBRARY}
                                   dl)
    math(EXPR D "${TEST_COUNTER}-1")
    add_dependencies(Graphics __TEST_DUMMY${D})

    add_test(GraphicsTest Graphics)
    # No memory test for this
endif()

