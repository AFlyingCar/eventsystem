/**
 * @file EventBase.h
 * @author Tyler Robbins
 *
 * @brief This file describes the base class from which all Events must derive.
 *
 * @par Usage
 * To define a custom event, a class or structure must be made that extends
 *      EventBase like so:
 * @code
 * struct MyEvent: public EventBase {
 *     PARENT(EventBase); // Optional
 * };
 * @endcode
 * @par
 * Any additional information may be placed in <code>MyEvent</code>, however it
 * is not required. The use of PARENT(BaseEvent) is optional, but recommended,
 * as it is required for the event system to allow polymorphic events, for
 * example:
 * @code
 * struct WindowEvent: public EventBase {
 *     PARENT(EventBase);
 * };
 * struct WindowMoveEvent: public WindowEvent {
 *     PARENT(WindowEvent);
 * };
 * @endcode
 * The above code allows a <code>WindowMoveEvent</code> to be fired, and have
 * all <code>WindowEvent</code> and <code>EventBase</code> subscribers to be
 * called as well.
 *
 * @par Copyright
 */

#ifndef EVENTBASE_H
#define EVENTBASE_H

#include <string> // std::string

namespace EventSystem3 {
    enum class Priority;

    /**
     * @brief The class from which all custom Event types must derive from.
     */
    class EventBase {
        public:
            /**
             * @brief The parent type of this class, is void by default and can
             *        be overridden by derived classes.
             */
            using parent_t = void;

            EventBase();
            EventBase(const EventBase&);
            EventBase(EventBase&&);
            virtual ~EventBase();

            Priority getPriority() const;

            void setTracebackInformation(const std::string&, unsigned long);
            void setPriority(Priority);

            const std::string& getFunction() const;
            unsigned long getLine() const;
        private:
            //! The priority of this event.
            Priority m_priority;

            //! The function this event was fired from.
            std::string m_func;

            //! The line number this event was fired from.
            unsigned long m_line;
    };
}

/**
 * @brief A helper macro wrapping around the specifics on how parent usages
 *        work.
 */
#define PARENT(P) using parent_t = P

#endif

