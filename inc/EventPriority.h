#ifndef EVENT_PRIORITY_H
#define EVENT_PRIORITY_H
namespace EventSystem3 {
    enum class Priority {
        LOWEST,
        LOW,
        NORMAL,
        HIGH,
        HIGHEST
    };
}

#define ES3_PRI_() NORMAL

#define ES3_PRI_LOW() LOW
#define ES3_PRI_LOWEST() LOWEST
#define ES3_PRI_NORMAL() NORMAL
#define ES3_PRI_HIGH() HIGH
#define ES3_PRI_HIGHEST() HIGHEST


#endif

