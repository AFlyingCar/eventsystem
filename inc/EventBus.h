/**
 * @file EventBus.h
 * @author Tyler Robbins
 *
 * @brief This file describes the main controller for the Event System.
 * @details In order to interact with the Event System, this file must be
 *          included. Usage of the EventSystem is described below.
 *
 * @par Every loop of the application, the Event System's update loop must be
 *      called with <code>EVENT_BUS.update(dt);</code>, where <code>dt</code> is
 *      the change in time since the last update loop was called. If this is set
 *      to 0, then delayed events will never fire.
 *
 * @par To fire an event, either the Event Bus's fire method can be called
 *      directly, or the EVENT_BUS_FIRE macro can be called instead.
 * @code
 * // Fire MyEvent using the fire method.
 * EVENT_BUS.fire(new MyEvent(), EventSystem3::Priority::HIGH);
 * // Fire MyEvent using the EVENT_BUS_FIRE macro:
 * EVENT_BUS_FIRE(new MyEvent(), HIGH,);
 * @endcode
 *
 * @par The EVENT_BUS_FIRE macro is recommended as it includes traceback
 *      information automatically and handles namespace resolution for priority
 *      specifiers. However, do not that when using the EVENT_BUS_FIRE macro,
 *      two commas are required, even if the final optional TIME parameter is
 *      ommitted.
 *
 * @par To subscribe a handler to an event, it is recommended to use the
 *      SUBSCRIBE macro defined in inc/EventSubscribe.h, however should that
 *      macro be unable to work, the subscribe method can be used as such:
 * @code
 * // Assume a class MyEventHandler exists which inherits from EventHandler
 * // and has a handler method `void handle(MyEvent&);`
 * MyEventHandler myHandlerObj;
 * EVENT_BUS.subscribe(&myHandlerObj, &MyEventHandler::handle, Priority::HIGH,
 *                     "handler");
 *
 * // Assume a function myHandler exists of signature `void myHandler(MyEvent&);`
 * EVENT_BUS.subscribe(myHandler, Priority::HIGH, "myHandler");
 * @endcode
 *
 * @par To unsubscribe a handler from an event, it is recommended to use the
 *      UNSUBSCRIBE macro defined in inc/EventSubscribe.h, however should that
 *      macro be unable to work, the unsubscribe method can be used as such:
 * @code
 * // Assume the previous code sample is still subscribed.
 * EVENT_BUS.unsubscribe<MyEvent>(&myHandlerObj, "handler");
 * EVENT_BUS.unsubscribe<MyEvent>("myHandler");
 * @endcode
 */

#ifndef EVENTBUS_H
#define EVENTBUS_H

#include <iostream> // DEBUGGING
#include <algorithm> // std::sort
#include <queue> // std::priority_queue
#include <functional> // std::function
#include <vector> // std::vector
#include <map> // std::map
#include <typeinfo> // typeid
#include <typeindex> // std::type_index
#include <mutex> // std::mutex
#include <memory> // std::shared_ptr

#include "EventBase.h" // EventBase
#include "EventFireError.h" // EventFireError
#include "EventHandler.h" // EventHandler
#include "EventPriority.h" // Priority

namespace EventSystem3 {
    /**
     * @brief A wrapper around an event subscriber, containing information about
     *        the subscriber.
     */
    struct FunctionWrapper {
        //! The function being wrapped around.
        std::function<void(EventBase*)> function;
        //! Whether this function has been unsubscribed.
        bool unsubscribed = false;
        //! The priority of this event subscriber.
        Priority priority;
        //! The name of this event subscriber.
        std::string name;
        /**
         * @brief A pointer to the object that this subscriber is a method of
         *         if applicable.
         */
        const void* obj_ptr;
    };
}

namespace std {
    /**
     * @brief An overload of greater for FunctionWrapper pointers.
     */
    template<>
    struct greater<EventSystem3::FunctionWrapper*> {
        /**
         * @brief Compares two FunctionWrapper* by priority.
         *
         * @param f1 The first FunctionWrapper to check.
         * @param f2 The second FunctionWrapper to check.
         *
         * @return True if f1 has a higher priority than f2, false otherwise.
         */
        bool operator()(const EventSystem3::FunctionWrapper* f1,
                        const EventSystem3::FunctionWrapper* f2) const
        {
            return f1->priority > f2->priority;
        }
    };
}

namespace EventSystem3 {
    /**
     * @brief The main controller of the Event System.
     */
    class EventBus final {
        //! The type of pointer to use for user-defined Events.
        template<typename T>
        using UserEventPointerType = std::shared_ptr<T>;

        //! The type of pointer to use for Events.
        using EventPointerType = UserEventPointerType<EventBase>;

        //! A container of handlers.
        using HandlerContainer = std::vector<FunctionWrapper*>;
        //! A mapping of types to a container for Handlers.
        using HandlerMap = std::map<std::type_index, HandlerContainer>;
        //! The data structure for holding the event queue.
        using EventQueue = std::priority_queue<EventPointerType,
                                               std::vector<EventPointerType>,
                                               std::greater<EventPointerType>>;
        public:
            static EventBus& getInstance();

            ~EventBus();

            // Deleted
            EventBus(const EventBus&) = delete;

        private:
            /**
             * @brief Allocates an event.
             * @details A helper to convert a tuple to constructor arguments.
             *
             * @tparam T The event type to allocate.
             * @tparam Args The argument types that are to be given to the type
             *         T.
             * @tparam I The indices
             *
             * @param eventArgs The arguments to give to the
             *
             * @return A dynamically allocated event of type T.
             */
            template<typename T, typename... Args, size_t... I>
            UserEventPointerType<T> AllocEvent(const std::tuple<Args...>& eventArgs,
                                        const std::index_sequence<I...>&)
            {
                return std::make_shared<T>(std::get<I>(eventArgs)...);
            }

            /**
             * @brief Allocates an event.
             * @details A helper to create a new type T.
             *
             * @tparam T the event type to allocate.
             *
             * @return A dynamically allocated event of type T.
             */
            template<typename T>
            UserEventPointerType<T> AllocEvent(const std::tuple<>&,
                          const std::index_sequence<>&)
            {
                return std::make_shared<T>();
            }

        public:
            /**
             * @brief Fires an event.
             * @details If T extends any other event type, then a new event will
             *          be fired for each type down the hierarchy.
             *
             * @tparam T The event type to fire.
             * @param eventArgs A std::tuple of arguments that can be used to
             *                  construct the event of type T.
             * @param priority The priority to fire the event with. Defaults to
             *                 NORMAL.
             * @param time The amount of time to wait before firing this event
             *             in seconds. Defaults to 0s.
             * @param func The name of the function this was called from. Used
             *             for debugging. Defaults to nothing.
             * @param line The line number this was called from. Used for
             *             debugging. Defaults to 0.
             */
            template<typename T, size_t Depth = 0, typename ... Args,
                     typename Idxs = std::make_index_sequence<sizeof...(Args)>>
            void fire(const std::tuple<Args...>& eventArgs,
                      Priority priority = Priority::NORMAL,
                      float time = 0.0f, const std::string& func = "",
                      unsigned long line = 0)
            {
                // Make sure that T extends from EventBase.
                static_assert(std::is_base_of<EventBase, T>::value,
                              "Cannot fire an object which does not extend "
                              "from EventBase.");
                UserEventPointerType<T> event = AllocEvent<T>(eventArgs, Idxs{});

                if constexpr(Depth == 0) {
                    m_update_mutex.lock();

#ifdef EVENTSYSTEM_DEBUG_PRINTING
                    std::cout << "Locking update mutex at depth " << Depth
                              << ", Type = " << typeid(T).name() << std::endl;
#endif
                }

#ifdef EVENTSYSTEM_DEBUG_PRINTING
                for(size_t i = 0; i < Depth; ++i) std::cout << ' ';
                std::cout << "fire " << typeid(T).name() << std::endl;
#endif

                // Make sure T is not void
                if constexpr (!std::is_same<T, void>::value) {
                    // Do not fire a nullptr
                    if(event == nullptr)
                        throw EventFireError(EventFireError::Type::NULLPTR,
                                             priority, time);

                    event->setTracebackInformation(func, line);
                    event->setPriority(priority);

                    // Ensure thread safety
                    if(time != 0.0f) {
                        m_wait_list.push_back(std::make_tuple(event, time));

#ifdef EVENTSYSTEM_DEBUG_PRINTING
                        std::cout << "Unlocking early: time{" << time
                                  << "} != 0.0f." << std::endl;
#endif

                        // We return early here, so make sure we unlock the
                        //  mutex so that other threads can still call fire()
                        m_update_mutex.unlock();
                        return;
                    }

                    m_event_queue.push(event);

                    // If we haven't reached the bottom of the hierarchy, then
                    //  fire another event of parent_t
                    if constexpr (!std::is_same<typename T::parent_t, void>::value)
                    {
                        fire<typename T::parent_t,
                             Depth + 1>(std::tuple<T>{*event},
                                        priority, time, func, line);
                    } else {
#ifdef EVENTSYSTEM_DEBUG_PRINTING
                        std::cout << "Unlocking update mutex at depth "
                                  << Depth << std::endl;
#endif

                        // We unlock the mutex here at the bottom of the
                        //   recursive chain
                        m_update_mutex.unlock();
                    }
                }
            }

            /**
             * @brief Fires an event.
             * @details If T extends any other event type, then a new event will
             *          be fired for each type down the hierarchy.
             *
             * @tparam T The event type to fire.
             *
             * @param priority The priority to fire the event with. Defaults to
             *                 NORMAL.
             * @param time The amount of time to wait before firing this event
             *             in seconds. Defaults to 0s.
             * @param func The name of the function this was called from. Used
             *             for debugging. Defaults to nothing.
             * @param line The line number this was called from. Used for
             *             debugging. Defaults to 0.
             */
            template<typename T, size_t Depth = 0>
            void fire(Priority priority = Priority::NORMAL,
                      float time = 0.0f, const std::string &func = "",
                      unsigned long line = 0)
            {
                fire<T>(std::tuple<>{}, priority, time, func, line);
            }

            void clear(); // DEBUGGING PURPOSES ONLY
            void update(float);

            /**
             * @brief Subscribes a function pointer to an event.
             *
             * @tparam E The event type to subscribe.
             * @param handler The function pointer to subscribe as a handler.
             * @param p The priority to give this handler.
             * @param name The name of the handler.
             */
            template<typename E>
            void subscribe(void(*handler)(E&), Priority p,
                           const std::string& name)
            {
                m_update_mutex.lock();

                // Add the function pointer to the map of handlers
                m_handlers_map[typeid(E)].push_back(
                        new FunctionWrapper{
                         // The function stored is a light lambda wrapper around
                         //  the original function.
                         [handler](EventBase* event) {
                             handler(*reinterpret_cast<E*>(event));
                         }, false, p, name, 0
                        });
                // Make sure the list of handlers stays sorted.
                std::sort(m_handlers_map[typeid(E)].begin(),
                     m_handlers_map[typeid(E)].end(),
                     std::greater<FunctionWrapper*>());

                m_update_mutex.unlock();
            }

            /**
             * @brief Subscribes a function object to an event.
             *
             * @tparam E The event type to subscribe.
             * @param handler The function to subscribe as a handler.
             * @param p The priority to give this handler.
             * @param name The name of the handler.
             */
            template<typename E>
            void subscribe(std::function<void(E&)>& handler, Priority p,
                           const std::string& name)
            {
                m_update_mutex.lock();

                m_handlers_map[typeid(E)].push_back(
                        new FunctionWrapper{
                         // The function stored is a light lambda wrapper around
                         //  the original function.
                         [handler](EventBase* event) {
                             handler(*reinterpret_cast<E*>(event));
                         }, false, p, name, 0
                        });
                // Make sure the list of handlers stays sorted.
                std::sort(m_handlers_map[typeid(E)].begin(), m_handlers_map[typeid(E)].end(), std::greater<FunctionWrapper*>());

                m_update_mutex.unlock();
            }

            /**
             * @brief Subscribes a method to an event.
             *
             * @tparam T The object type to subscribe
             * @tparam E The event type to subscribe to.
             *
             * @param obj The EventHandler object that owns the method.
             * @param handler The method to subscribe as a handler.
             * @param p The priority to give this handler.
             * @param name The name of the handler.
             */
            template<typename T, typename E>
            void subscribe(EventHandler* obj, void(T::*handler)(E&), Priority p,
                           const std::string& name)
            {
                m_update_mutex.lock();

                // TODO: Add static_assert check for inheritance on EventHandler and T
                FunctionWrapper* fw = new FunctionWrapper{
                         [obj, handler](EventBase* event) {
                             (reinterpret_cast<T*>(obj)->*(handler))(*reinterpret_cast<E*>(event));
                         },
                         false,
                         p,
                         name,
                         obj
                        };
                m_handlers_map[typeid(E)].push_back(fw);
                std::sort(m_handlers_map[typeid(E)].begin(), m_handlers_map[typeid(E)].end(), std::greater<FunctionWrapper*>());
                obj->setFWrapper(fw);

                m_update_mutex.unlock();
            }

            /**
             * @brief Unsubscribes a named handler from an event.
             *
             * @tparam E The event type to unsubscribe from.
             * @param name The name of the handler to unsubscribe.
             */
            template<typename E>
            void unsubscribe(const std::string& name) {
                unsubscribe<E>(nullptr, name);
            }

            /**
             * @brief Unsubscribes a name method from an event.
             *
             * @tparam E The event type to unsubscribe from.
             * @param ptr The object which should be unsubscribed
             * @param name The name of the handler to unsubscribe.
             */
            template<typename E>
            void unsubscribe(void* ptr, const std::string& name) {
                m_update_mutex.lock();

                for(auto& fw : m_handlers_map[typeid(E)]) {
                    if(fw->obj_ptr == ptr && fw->name == name) {
                        fw->unsubscribed = true;
                    }
                }

                m_update_mutex.unlock();
            }

            size_t getEventQueueSize() const;
            size_t getWaitingEventListSize() const;
            size_t getHandlersFor(std::type_index) const;
        private:
            EventBus();

            //! The queue of active events.
            EventQueue m_event_queue;

            //! A mapping of all subscribed handlers.
            HandlerMap m_handlers_map;

            // TODO: Priority?
            //! A list of all events waiting on timers.
            std::vector<std::tuple<EventPointerType, float>> m_wait_list;

            //! A mutex to allow thread safety with update()
            std::mutex m_update_mutex;
    };
}

/**
 * @brief Returns the value given.
 * @details used for EVENT_BUS_FIRE.
 * @param v An integer value.
 * @return The value given.
 */
[[maybe_unused]] static int __0_if_no_value(int v) { return v; }
/**
 * @brief Returns 0
 * @details used for EVENT_BUS_FIRE.
 * @return 0
 */
[[maybe_unused]] static int __0_if_no_value() { return 0; }

/**
 * @brief A light wrapper around the single instance of the EventBus.
 */
#define EVENT_BUS EventSystem3::EventBus::getInstance()

/**
 * @brief A wrapper around firing an event.
 * @details All 3 parameters must exist, but the TIME parameter can be left
 *          blank: <code>EVENT_BUS_FIRE(new EventBase(), HIGH,);</code>
 *
 * @param EVENTTYPE The type of event to fire.
 * @param ARGS The arguments to give to the event once it has been created.
 * @param PRIORITY The priority to fire.
 * @param TIME The delay on the event.
 */
#define EVENT_BUS_FIRE(EVENTTYPE, ARGS, PRIORITY, TIME)                        \
    EVENT_BUS .fire<EVENTTYPE>(std::make_tuple(ARGS),                          \
                    EventSystem3::Priority:: CONCAT(ES3_PRI_, PRIORITY)(), \
                    __0_if_no_value(TIME), __FUNCTION__, __LINE__)

#endif

