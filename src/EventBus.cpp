/**
 * @file EventBus.cpp
 * @author Tyler Robbins
 *
 * @brief Defines all methods of EventSystem3::EventBus as defined in EventBus.h
 */

#include <iostream>

#include "EventBus.h"

/**
 * @brief Constructs the EventBus.
 */
EventSystem3::EventBus::EventBus(): m_event_queue(),
                                    m_handlers_map(),
                                    m_wait_list(),
                                    m_update_mutex()
{
}

/**
 * @brief Gets the single instance of this EventBus.
 *
 * @return The single instance of this EventBus.
 */
EventSystem3::EventBus& EventSystem3::EventBus::getInstance() {
    static EventBus instance;
    return instance;
}

/**
 * @brief Destroys this EventBus.
 * @details As EventBus is a singleton, in most circumstances this method will
 *          never be called and is simply here to showcase how an EventBus would
 *          be destroyed if it were not a singleton.
 */
EventSystem3::EventBus::~EventBus() {
    // Make sure that there are no events left in the queue.
    clear();
    for(auto& pair : m_handlers_map) {
        for(auto& fw : pair.second) {
            // Destroy ever function wrapper
            delete fw;
        }
        // TODO: Is this actually necessary?
        pair.second.clear();
    }
}

/**
 * @brief Clears the entire event queue.
 */
void EventSystem3::EventBus::clear() {
    while(!m_event_queue.empty()) {
        // Delete every EventBase from the event queue while removing them.
        EventPointerType eb = m_event_queue.top();
        m_event_queue.pop();
    }
}

/**
 * @brief The main update loop of the event bus which controls the entire logic
 *        of the bus.
 *
 * @param dt Change in time since the last update call.
 */
void EventSystem3::EventBus::update(float dt) {
    m_update_mutex.lock();

    // First we deal with every event in the queue.
    while(!m_event_queue.empty()) {
        EventPointerType ept = m_event_queue.top();
        EventBase* eb = ept.get();
        m_event_queue.pop();

        if(m_handlers_map.count(typeid(*eb)) > 0) {
            // Make sure to get a copy of it for thread safety
            HandlerContainer handlers = m_handlers_map.at(typeid(*eb));
            for(size_t i = 0; i < handlers.size(); ++i) {
                // Check if it is unsubscribed first, as with object handlers
                //  this could cause us to use an invalidated/deleted pointer.
                if(handlers[i]->unsubscribed) {
                    delete handlers.at(i);
                    m_handlers_map[typeid(*eb)].erase(m_handlers_map[typeid(*eb)].begin() + (i--));
                    handlers = m_handlers_map[typeid(*eb)];
                } else {
                    handlers[i]->function(eb);
                }
            }
        }
    }

    // Now we handle the waiting list, decreasing their wait times and moving
    //  them to the event queue if the time to wait is up.
    for(size_t i = 0; i < m_wait_list.size(); ++i) {
        if((std::get<1>(m_wait_list[i]) -= dt) <= 0.0f) {
            m_event_queue.push(std::get<0>(m_wait_list[i]));
            m_wait_list.erase(m_wait_list.begin() + i);
            --i;
        }
    }

    m_update_mutex.unlock();
}

/**
 * @brief Gets the size of the event queue.
 *
 * @return The size of the event queue.
 */
size_t EventSystem3::EventBus::getEventQueueSize() const {
    return m_event_queue.size();
}

/**
 * @brief Gets the size of the waiting list.
 *
 * @return The size of the waiting list.
 */
size_t EventSystem3::EventBus::getWaitingEventListSize() const {
    return m_wait_list.size();
}

/**
 * @brief Gets the number of event handlers for the given EventType represented
 *        as a <code>std::type_index</code>.
 *
 * @param idx The EventType to get all event handlers for.
 *
 * @return The number of event handlers for the given EventType.
 */
size_t EventSystem3::EventBus::getHandlersFor(std::type_index idx) const {
    return m_handlers_map.at(idx).size();
}

