/**
 * @file EventBase.cpp
 * @author Tyler Robbins
 *
 * @brief Defines all methods for EventSystem3::EventBase as described in
 *        EventBase.h.
 */

#include "EventBase.h"
#include "EventBus.h"

/**
 * @brief Constructs this event with NORMAL priority.
 */
EventSystem3::EventBase::EventBase(): m_priority(Priority::NORMAL) {
}

/**
 * @brief Copies data from another event.
 */
EventSystem3::EventBase::EventBase(const EventBase&)
{ // TODO: Incomplete?
}

/**
 * @brief Moves data from another event to this one.
 *
 * @param other The other event to copy from.
 */
EventSystem3::EventBase::EventBase(EventBase&& other):
    m_priority(std::move(other.m_priority))
{
}

/**
 * @brief Destroys this event.
 */
EventSystem3::EventBase::~EventBase() { }

/**
 * @brief Gets the priority of this event.
 *
 * @return This event's priority.
 */
EventSystem3::Priority EventSystem3::EventBase::getPriority() const {
    return m_priority;
}

/**
 * @brief Sets the priority of this event.
 *
 * @param priority The priority to set this event's priority to.
 */
void EventSystem3::EventBase::setPriority(EventSystem3::Priority priority)
{
    m_priority = priority;
}

/**
 * @brief Sets the traceback information of this event.
 *
 * @param func The name of the function this event was fired from.
 * @param line The line number this event was fired on.
 */
void EventSystem3::EventBase::setTracebackInformation(const std::string& func,
                                                      unsigned long line)
{
    if(!func.empty()) m_func = func;
    if(line > 0) m_line = line;
}

/**
 * @brief Gets the function name this event was fired from.
 *
 * @return The name of the function this event was fired from.
 */
const std::string& EventSystem3::EventBase::getFunction() const {
    return m_func;
}

/**
 * @brief Gets the line nubmer this event was fired on.
 * 
 * @return The line number this event was fired on.
 */
unsigned long EventSystem3::EventBase::getLine() const {
    return m_line;
}

