#include <iostream>

#include "EventBus.h"
#include "EventBase.h"
#include "EventSubscribe.h"
#include "EventHandler.h"

struct MyEvent: public EventSystem3::EventBase {
    MyEvent(int i): m_i(i) { }
    int m_i;
    PARENT(EventBase);
};

struct OtherEvent: public EventSystem3::EventBase {
    OtherEvent(int i): m_i(i) { }
    int m_i;
    PARENT(EventBase);
};

void myHandler(MyEvent& e) {
    std::cout << "myHandler(" << e.m_i << "): " << (int)e.getPriority() << std::endl;
}
SUBSCRIBE(MyEvent, myHandler,,);

void myHandler2(MyEvent& e) {
    std::cout << "myHandler2(" << e.m_i << "): " << (int)e.getPriority() << std::endl;
}
SUBSCRIBE(MyEvent, myHandler2,, HIGHEST);

void otherHandler(OtherEvent& e) {
    std::cout << "otherHandler(" << e.m_i << "): " << (int)e.getPriority() << std::endl;
}
SUBSCRIBE(OtherEvent, otherHandler,,LOWEST);

int main(int, char**) {
    EVENT_BUS_FIRE(MyEvent,(0),,);

    EVENT_BUS.update(0);

    std::cout << "====================" << std::endl;

    EVENT_BUS_FIRE(OtherEvent,(0),LOWEST,);
    EVENT_BUS_FIRE(MyEvent,(1),,);
    EVENT_BUS_FIRE(OtherEvent,(2),HIGHEST,);
    EVENT_BUS.update(0);
}

