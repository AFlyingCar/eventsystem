#include <iostream>

#ifdef _WIN32
#include <Windows.h>
// https://stackoverflow.com/questions/5801813/c-usleep-is-obsolete-workarounds-for-windows-mingw
void usleep(__int64 usec) {
    HANDLE timer;
    LARGE_INTEGER ft;

    ft.QuadPart = -(10 * usec); // Convert to 100 nanosecond interval, negative value indicates relative time

    timer = CreateWaitableTimer(NULL, TRUE, NULL);
    SetWaitableTimer(timer, &ft, 0, NULL, NULL, 0);
    WaitForSingleObject(timer, INFINITE);
    CloseHandle(timer);
}
#else
#include <unistd.h>
#endif

#include "EventBus.h"
#include "EventBase.h"
#include "EventSubscribe.h"
#include "EventHandler.h"

struct QuitEvent: public EventSystem3::EventBase {
    PARENT(EventBase);
};

struct MyEvent: public EventSystem3::EventBase {
    PARENT(EventBase);
};

bool do_quit = false;

void handleQuit(QuitEvent&) {
    do_quit = true;
}
SUBSCRIBE(QuitEvent, handleQuit,,);

void handleMyEvent(MyEvent&) {
    std::cout << "handleMyEvent" << std::endl;
}
SUBSCRIBE(MyEvent, handleMyEvent,,);

int main(int, char**) {
    EVENT_BUS_FIRE(QuitEvent,,,5.0f);
    EVENT_BUS_FIRE(MyEvent,,HIGHEST,5.0f);
    while(!do_quit) {
        EVENT_BUS.update(1);
        usleep(1000000);    // Simulate an actual delay for dt
    }

    std::cout << "Quitting." << std::endl;
}

