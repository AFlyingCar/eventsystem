#include <iostream>
#include <thread>

#include "EventBus.h"
#include "EventBase.h"
#include "EventSubscribe.h"

struct EventA: public EventSystem3::EventBase {
    PARENT(EventBase);
};

struct EventB: public EventA {
    PARENT(EventA);
};

struct EventC: public EventB {
    PARENT(EventB);
};

struct MyEvent: public EventC {
    PARENT(EventC);
};

struct MyOtherEvent: public EventB {
};

void handler(MyEvent&) {
    std::cout << "Handling MyEvent" << std::endl;
}
SUBSCRIBE(MyEvent, handler,,);

void handler2(MyOtherEvent&) {
    std::cout << "Handling MyOtherEvent" << std::endl;
}
SUBSCRIBE(MyOtherEvent, handler2,,);

void handler3(MyEvent&) {
    std::cout << "Handling MyEvent again" << std::endl;
}
SUBSCRIBE(MyEvent, handler3,,);

void handler4(EventC&) {
    std::cout << "Handling EventC" << std::endl;
}
SUBSCRIBE(EventC, handler4,,);

int main() {
    bool t1_done = false;
    bool t2_done = false;
    bool t3_done = false;
    bool while_started = false;

    std::thread t1([&t1_done, &while_started]() {
        while(!while_started) std::this_thread::yield();
        for(int i = 0; i < 10; ++i) {
            EVENT_BUS_FIRE(MyEvent,,,);
        }
        t1_done = true;
    });

    std::thread t2([&t2_done, &while_started]() {
        while(!while_started) std::this_thread::yield();
        for(int i = 0; i < 10; ++i) {
            EVENT_BUS_FIRE(MyOtherEvent,,,);
        }
        t2_done = true;
    });

    std::thread t3([&t3_done, &while_started]() {
        while(!while_started) std::this_thread::yield();

        for(int i = 0; i < 10; ++i) {
            EVENT_BUS_FIRE(EventC,,,);
        }
        t3_done = true;
    });

    while(!t1_done || !t2_done || !t3_done) {
        while_started = true;
        EVENT_BUS.update(0.0f);
    }

    t1.join();
    t2.join();
    t3.join();

    return 0;
}

