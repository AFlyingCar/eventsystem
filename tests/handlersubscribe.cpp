#include <iostream>

#include "EventBus.h"
#include "EventBase.h"
#include "EventSubscribe.h"

struct MyEvent: public EventSystem3::EventBase {
    PARENT(EventBase);
};

struct OtherEvent: public EventSystem3::EventBase {
    PARENT(EventBase);
};

void ohandler(OtherEvent&) {
    std::cout << "Handling OtherEvent" << std::endl;
}
SUBSCRIBE(OtherEvent, ohandler,,);

void handler(MyEvent&) {
    std::cout << "Handling MyEvent" << std::endl;
}
SUBSCRIBE(MyEvent, handler,,);

void handler2(EventSystem3::EventBase&) {
    std::cout << "Handling generic EventBase." << std::endl;
}

int main(int, char**) {
    SUBSCRIBE(EventSystem3::EventBase, handler2,,);

    std::cout << "============================================" << std::endl;
    std::cout << "====          Firing OtherEvent         ====" << std::endl;
    std::cout << "============================================" << std::endl;
    for(int i = 0; i < 10; ++i) {
        // EVENT_BUS.fire(new OtherEvent());
        EVENT_BUS_FIRE(OtherEvent,,,);
        EVENT_BUS.update(0.0f);
    }
    std::cout << "============================================" << std::endl;
    std::cout << "====            Firing MyEvent          ====" << std::endl;
    std::cout << "============================================" << std::endl;
    for(int i = 0; i < 10; ++i) {
        // EVENT_BUS.fire(new MyEvent());
        EVENT_BUS_FIRE(MyEvent,,,);
        EVENT_BUS.update(0.0f);
    }

    std::cout << "============================================" << std::endl;
    std::cout << "==== Unsubscribing handler from MyEvent ====" << std::endl;
    std::cout << "============================================" << std::endl;

    EVENT_BUS.clear();
    UNSUBSCRIBE(MyEvent, handler, );
    for(int i = 0; i < 10; ++i) {
        // EVENT_BUS.fire(new MyEvent());
        EVENT_BUS_FIRE(MyEvent,,,);
        EVENT_BUS.update(0.0f);
    }
    return 0;
}

