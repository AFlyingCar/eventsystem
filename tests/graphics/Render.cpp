/**
 * @file    Render.cpp
 * @date    2018-01-23
 *
 * @author  Tyler Robbins
 * 
 * @par Term:       Spring
 * @par Course:     CS250-A
 * @par Assignment: #02
 */

#include "Render.h"

#include <stdexcept> // std::runtime_error

#include <GL/glew.h>
#include <GL/gl.h>

#include "Affine.h"

Render::Render() {
    GLint value;

    // compile fragment shader
    const char* fragment_shader_text =
        "#version 130\n\
         uniform vec3 color;\
         out vec4 frag_color;\
         void main(void) {\
           frag_color = vec4(color,1);\
         }";

    GLuint fshader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fshader, 1, &fragment_shader_text, 0);
    glCompileShader(fshader);
    glGetShaderiv(fshader, GL_COMPILE_STATUS, &value);

    if(!value) {
        char buffer[1024];
        glGetShaderInfoLog(fshader, 1024, 0, buffer);
        throw std::runtime_error(buffer);
    }

    // compile vertex shader
    const char* vertex_shader_text =
        "#version 130\n\
     attribute vec4 position;\
     void main() {\
       gl_Position = position;\
     }";
    GLuint vshader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vshader, 1, &vertex_shader_text, 0);
    glCompileShader(vshader);
    glGetShaderiv(vshader, GL_COMPILE_STATUS, &value);

    if(!value) {
        char buffer[1024];
        glGetShaderInfoLog(vshader, 1024, 0, buffer);
        throw std::runtime_error(buffer);
    }

    // link shader program
    program = glCreateProgram();
    glAttachShader(program, fshader);
    glAttachShader(program, vshader);
    glLinkProgram(program);
    glGetProgramiv(program, GL_LINK_STATUS, &value);

    if(!value) {
        char buffer[1024];
        glGetProgramInfoLog(program, 1024, 0, buffer);
        throw std::runtime_error(buffer);
    }

    glDeleteShader(vshader);
    glDeleteShader(fshader);
    // enable z-buffer
    glEnable(GL_DEPTH_TEST);
    // get shader parameter locations
    aposition = glGetAttribLocation(program, "position");
    ucolor = glGetUniformLocation(program, "color");
    // "turn-on" the position shader attribute
    glEnableVertexAttribArray(aposition);
}

Render::~Render() {
  // delete shader program
  glUseProgram(0);
  glDeleteProgram(program);
}

void Render::clearBuffers(const Vector& c) {
    glClearColor(c.x,c.y,c.z,1);
    glClear(GL_COLOR_BUFFER_BIT);
    glClearDepth(1);
    glClear(GL_DEPTH_BUFFER_BIT);
}

void Render::setColor(const Vector& c) {
    glUseProgram(program);
    glUniform3f(ucolor,c[0],c[1],c[2]);
}

void Render::drawLine(const Hcoord& P, const Hcoord& Q) {
    glUseProgram(program);

    float line_vertex[] = { P[0],P[1],P[2],P[3],  Q[0],Q[1],Q[2],Q[3] };

    GLuint vertex_buffer;
    glGenBuffers(1, &vertex_buffer);

    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);

    glBufferData(GL_ARRAY_BUFFER, sizeof(line_vertex), line_vertex,
                 GL_STATIC_DRAW);

    glVertexAttribPointer(aposition, 4, GL_FLOAT, false, 4 * sizeof(float), 0);

    glDrawArrays(GL_LINES, 0, 2);

    glDeleteBuffers(1, &vertex_buffer);
}

void Render::fillTriangle(const Hcoord& P, const Hcoord& Q, const Hcoord& R) {
    glUseProgram(program);
    GLuint vertex_buffer;

    float face_vertices[] = { P[0], P[1], P[2], P[3],
                              Q[0], Q[1], Q[2], Q[3],
                              R[0], R[1], R[2], R[3]
                            };

    glGenBuffers(1, &vertex_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(face_vertices), face_vertices,
                 GL_STATIC_DRAW);
    glVertexAttribPointer(aposition, 4, GL_FLOAT, false, 4 * sizeof(float), 0);
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glDeleteBuffers(1, &vertex_buffer);
}

