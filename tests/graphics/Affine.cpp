/**
 * @file    Affine.cpp
 * @date    2018-01-06
 *
 * @author  Tyler Robbins
 * 
 * @par Term:       Fall
 * @par Course:     CS250-A
 * @par Assignment: #01
 */

#include "Affine.h"

/**
 * @brief Gets a column from a matrix.
 *
 * @param m The matrix.
 * @param col The column #.
 *
 * @return An Hcoord of all values in the column
 */
Hcoord column(const Matrix& m, size_t col) {
    return Hcoord(m[0][col], m[1][col], m[2][col], m[3][col]);
}

/**
 * @brief v * v transpose
 *
 * @return A matrix of v * v transpose
 */
Matrix uut(const Vector& v) {
    Matrix m;
    m[0] = Hcoord(v[0] * v[0], v[0] * v[1], v[0] * v[2], 0);
    m[1] = Hcoord(v[1] * v[0], v[1] * v[1], v[1] * v[2], 0);
    m[2] = Hcoord(v[2] * v[0], v[2] * v[1], v[2] * v[2], 0);
    m[3] = Hcoord(0, 0, 0, 1);

    return m;
}

/**
 * @brief Constructs a Homogenous coordinate
 *
 * @param X The X component.
 * @param Y The Y component.
 * @param Z The Z component.
 * @param W The W component.
 */
Hcoord::Hcoord(float X, float Y, float Z, float W): x(X), y(Y), z(Z), w(W) {
}

/**
 * @brief Constructs a Point.
 *
 * @param X The X coordinate.
 * @param Y The Y coordinate.
 * @param Z The Z coordinate.
 */
Point::Point(float X, float Y, float Z): Hcoord(X, Y, Z, 1) {
}

/**
 * @brief Constructs a Vector.
 *
 * @param X The X component.
 * @param Y The Y component.
 * @param Z The Z component.
 */
Vector::Vector(float X, float Y, float Z): Hcoord(X, Y, Z, 0) {
}

/**
 * @brief Constructs an Affine transformation with default values.
 * @details Will construct the following transformation:
 *          | 0 0 0 0 |
 *          | 0 0 0 0 |
 *          | 0 0 0 0 |
 *          | 0 0 0 1 |
 */
Affine::Affine(void) {
    row[0] = Hcoord(0, 0, 0, 0);
    row[1] = Hcoord(0, 0, 0, 0);
    row[2] = Hcoord(0, 0, 0, 0);
    row[3] = Hcoord(0, 0, 0, 1);
}

/**
 * @brief Constructs an Affine transformation.
 * @details Will construct the following transformation:
 *          | Lx0 Ly0 Lz0 D0 |
 *          | Lx1 Ly1 Lz1 D1 |
 *          | Lx2 Ly2 Lz2 D2 |
 *          |  0   0   0  1  |
 *
 * @param Lx The first column.
 * @param Ly The second column.
 * @param Lz The third column.
 * @param disp The third column.
 */
Affine::Affine(const Vector& Lx, const Vector& Ly, const Vector& Lz,
               const Point& D)
{
    row[0] = Hcoord(Lx[0], Ly[0], Lz[0], D[0]);
    row[1] = Hcoord(Lx[1], Ly[1], Lz[1], D[1]);
    row[2] = Hcoord(Lx[2], Ly[2], Lz[2], D[2]);
    row[3] = Hcoord( 0,      0,     0,    1  );
}

/**
 * @brief Adds two homogenous coordinates.
 * @details Performs the following operation:
 *          | u.x |   | v.x |   | u.x + v.x |
 *          | u.y | + | v.y | = | u.y + v.y |
 *          | u.z |   | v.z |   | u.z + v.z |
 *          | u.w |   | v.w |   | u.w + v.w |
 *
 * @param u The first coordinate.
 * @param v The second coordinate.
 *
 * @return The sum of u and v.
 */
Hcoord operator+(const Hcoord& u, const Hcoord& v) {
    return Hcoord(u.x + v.x, u.y + v.y, u.z + v.z, u.w + v.w);
}

/**
 * @brief Subtracts two homogenous coordinates.
 * @details Performs the following operation:
 *          | u.x |   | v.x |   | u.x - v.x |
 *          | u.y | - | v.y | = | u.y - v.y |
 *          | u.z |   | v.z |   | u.z - v.z |
 *          | u.w |   | v.w |   | u.w - v.w |
 *
 * @param u The first coordinate.
 * @param v The second coordinate.
 *
 * @return The difference between u and v.
 */
Hcoord operator-(const Hcoord& u, const Hcoord& v) {
    return Hcoord(u.x - v.x, u.y - v.y, u.z - v.z, u.w - v.w);
}

/**
 * @brief Negates a homogenous coordinate.
 * @details Performs the following operation:
 *            | v.x |   | -v.x |
 *          - | v.y | = | -v.y |
 *            | v.z |   | -v.z |
 *            | v.w |   | -v.w |
 *
 * @param v The homogenous coordinate to negate.
 * @return The negated coordinate.
 */
Hcoord operator-(const Hcoord& v) {
    return Hcoord(-v.x, -v.y, -v.z, -v.w);
}

/**
 * @brief Multiplies a scalar constant by a homogenous coordinate.
 * @details Performs the following operation:
 *              | v.x |   | r * v.x |
 *          r * | v.y | = | r * v.y |
 *              | v.z |   | r * v.z |
 *              | v.w |   | r * v.w |
 *
 * @param r The scalar to multiply
 * @param v The coordinate to multiply.
 *
 * @return The coordinate multiplied by r.
 */
Hcoord operator*(float r, const Hcoord& v) {
    return Hcoord(v.x * r, v.y * r, v.z * r, v.w * r);
}

/**
 * @brief Multiplies a Matrix and an Hcoord.
 *
 * @param A The matrix.
 * @param v The hcoordinate
 *
 * @return A * v
 */
Hcoord operator*(const Matrix& A, const Hcoord& v) {
    Hcoord h;

    for(int i = 0; i < 4; i++) {
        float sum = (A[i][0] * v[0]) + (A[i][1] * v[1]) + (A[i][2] * v[2]) + (A[i][3] * v[3]);
        h[i] = sum;
    }

    return h;
}

/**
 * @brief Multiplies/concatenates two Matrices.
 *
 * @param A The first matrix.
 * @param B The second matrix.
 *
 * @return The product of A and B.
 */
Matrix operator*(const Matrix& A, const Matrix& B) {
    Matrix C;

    for(int i = 0; i < 4; i++) {
        for(int j = 0; j < 4; j++) {
            float sum = 0.0f;

            for(int k = 0; k < 4; k++) {
                sum += A[i][k] * B[k][j];
            }

            C[i][j] = sum;
        }
    }

    return C;
}

/**
 * @brief Adds two Matrices together.
 *
 * @param A The first matrix to add.
 * @param B The second matrix to add.
 *
 * @return The sum of A and B.
 */
Matrix operator+(const Matrix& A, const Matrix& B) {
    Matrix C;
    for(int i = 0; i < 4; ++i) {
        for(int j = 0; j < 4; ++j) {
            C[i][j] = A[i][j] + B[i][j];
        }
    }

    return C;
}

/**
 * @brief Dots two vectors together.
 * @details Performs the following operation:
 *          | u.x |   | v.x |
 *          | u.y | * | v.y | = (ux * ux) + (uy * uy) + (uz * vz) + (uw * uw)
 *          | u.z |   | v.z |
 *          | u.w |   | v.w |
 *
 * @param u The first vector.
 * @param v The second vector.
 *
 * @return The dot product of u and v.
 */
float dot(const Vector& u, const Vector& v) {
    return (u.x * v.x) + (u.y * v.y) + (u.z * v.z) + (u.w * v.w);
}

/**
 * @brief Gets the length of a vector.
 *
 * @param v The vector to get the length of.
 * @return The length of the vector v.
 */
float abs(const Vector& v) {
    return std::sqrt((v.x * v.x) + (v.y * v.y) + (v.z * v.z) + (v.w * v.w));
}

/**
 * @brief Normalizes a vector.
 *
 * @param v The vector to normalize.
 * @return The normalized vector.
 */
Vector normalize(const Vector& v) {
    return (1 / abs(v)) * v;
}

/**
 * @brief Calculates the cross product of two vectors
 * @details Creates the following Vector:
 *          | u1*v2 - u2*v1 |
 *          | u2*v0 - u0*v2 |
 *          | u0*v1 - u1*v0 |
 *          |       0       |
 *
 * @param u The first vector.
 * @param v The second vector.
 *
 * @return The result of u x v
 */
Vector cross(const Vector& u, const Vector& v) {
    return Vector((u[1] * v[2]) - (u[2] * v[1]),
                  (u[2] * v[0]) - (u[0] * v[2]),
                  (u[0] * v[1]) - (u[1] * v[0]));
}

/**
 * @brief Calculates a 3D rotation affine transformation.
 *
 * @param t The angle to rotate by.
 * @param v The axis of rotation.
 *
 * @return The 3D affine transformation for rotation by t around axis v.
 */
Affine rotate(float t, const Vector& v) {
    float c_t = std::cos(t);

    Matrix lambdaV;

    lambdaV[0] = Hcoord(0,    -v[2], v[1], 0);
    lambdaV[1] = Hcoord(v[2],   0,   -v[0], 0);
    lambdaV[2] = Hcoord(-v[1], v[0],  0,   0);
    lambdaV[3] = Hcoord(0, 0, 0, 1);

    Affine vvt = uut(v);

    float cvvt = (1 - c_t) / dot(v, v);
    float clv = std::sin(t) / abs(v);

    Affine cts = scale(c_t);
    Affine cvvts = scale(cvvt);
    Affine clvs = scale(clv);

    Affine cvvts_vvt = cvvts * vvt;
    Affine clvs_lambdaV = clvs * lambdaV;

    Matrix mat = cts + cvvts_vvt + clvs_lambdaV;
    mat[3][3] = 1;

    return mat;
}

/**
 * @brief Creates a translation Affine transformation.
 * @details Creates the following Affine transformation:
 *          | 1 0 0 v.x |
 *          | 0 1 0 v.y |
 *          | 0 0 1 v.z |
 *          | 0 0 0  1  |
 *
 * @param v The vector to translate to.
 * @return The new translation Affine transformation.
 */
Affine translate(const Vector& v) {
    return Affine(Vector(1, 0, 0),
                  Vector(0, 1, 0),
                  Vector(0, 0, 1),
                  Point(v.x, v.y, v.z));
}

/**
 * @brief Creates a uniform scaling Affine transformation.
 * @details Creates the following Affine Transformation:
 *          | r 0 0 0 |
 *          | 0 r 0 0 |
 *          | 0 0 r 0 |
 *          | 0 0 0 1 |
 *
 * @param r The value to scale by.
 * @return The new uniform scaling Affine transformation.
 */
Affine scale(float r) {
    return Affine(Vector(r, 0, 0),
                  Vector(0, r, 0),
                  Vector(0, 0, r),
                  Point( 0, 0, 0)
                 );
}

/**
 * @brief Creates a non-uniform scaling Affine transformation.
 * @details Creates the following Affine Transformation:
 *          | rx 0  0  0 |
 *          | 0  ry 0  0 |
 *          | 0  0  rz 0 |
 *          | 0  0  0  1 |
 *
 * @param rx The x-value to scale by.
 * @param ry The y-value to scale by.
 * @param rz The z-value to scale by.
 * @return The new non-uniform scaling Affine transformation.
 */
Affine scale(float rx, float ry, float rz) {
    return Affine(Vector(rx, 0,  0),
                  Vector(0,  ry, 0),
                  Vector(0,  0,  rz),
                  Point( 0,  0,  0)
                 );
}

/**
 * @brief Calculates the inverse of an Affine transformation.
 *
 * @param A The Affine transformation to invert.
 * @return The inverse of A.
 */
Affine inverse(const Affine& A) {
    Affine C;

    // For every row (Only care about the Linear part)
    for(int i = 0; i < 3; ++i) {
        // For every column (Only care about the Linear part)
        for(int j = 0; j < 3; ++j) {
            // Get the four values for our determinant calculation.
            float tl = A[(i + 1) % 3][(j + 1) % 3]; // Top Left
            float bl = A[(i + 2) % 3][(j + 1) % 3]; // Bottom Left
            float tr = A[(i + 1) % 3][(j + 2) % 3]; // Top Right
            float br = A[(i + 2) % 3][(j + 2) % 3]; // Bottom Right

            // Only the even indices are negative
            bool negative = false;//(((i + j) % 2) == 0);

            // The value is the determinant
            C[i][j] = (negative ? -1 : 1) * ((tl * br) - (bl * tr));
        }
    }

    // The inverse of the determinant
    float inv_det_A = 1 / dot(column(A, 0), column(C, 0));
    // float inv_det_A = 1 / dot(A[0], C[0]);

    // Make sure the last value for each column is 0
    Hcoord c1 = inv_det_A * C[0];
    c1[3] = 0;
    Hcoord c2 = inv_det_A * C[1];
    c2[3] = 0;
    Hcoord c3 = inv_det_A * C[2];
    c3[3] = 0;

    Affine linear_inverse(c1, c2, c3, Point(0, 0, 0));

    Vector translation_part(-A[0][3], -A[1][3], -A[2][3]);

    // Linear part inverse * translation part.
    Affine true_inverse = linear_inverse * translate(translation_part);

    return true_inverse;
}

