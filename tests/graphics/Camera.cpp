/**
 * @file    Camera.cpp
 * @date    2018-02-01
 *
 * @author  Tyler Robbins
 *
 * @par Term:       Spring
 * @par Course:     CS250-A
 * @par Assignment: #04
 */

// For Windows
#define _USE_MATH_DEFINES

#include "Camera.h"

namespace {
    Point O(0, 0, 0); //!< The Origin
}

Camera::Camera(): Camera(O, Vector(0, 0, -1), Vector(0, 1, 0), M_PI/4.0f, 1.0f,
                         0.1, 10)
{
}

Camera::Camera(const Point& E, const Vector& look, const Vector& rel, float fov,
               float aspect, float N, float F):
    eye_point(E),
    right_vector(), // Do this in the constructor body to save operations
    up_vector(),    //  ... Which means we need to skip this too.
    back_vector(-(1/abs(look)) * look),
    width(2*N*std::tan(fov/2.0f)),
    height(width/aspect),
    distance(N),
    near(N),
    far(F)
{
    Vector lr_cross = cross(look, rel);

    right_vector = (1/abs(lr_cross)) * lr_cross;
    up_vector = cross(back_vector, right_vector);
}

Point Camera::eye(void) const {
    return eye_point;
}

Vector Camera::right(void) const {
    return right_vector;
}

Vector Camera::up(void) const {
    return up_vector;
}

Vector Camera::back(void) const {
    return back_vector;
}

Vector Camera::viewportGeometry(void) const {
    return Vector(width, height, distance);
}

float Camera::nearDistance(void) const {
    return near;
}

float Camera::farDistance(void) const {
    return far;
}

Camera& Camera::zoom(float factor) {
    width *= factor;
    height *= factor;

    return *this;
}

Camera& Camera::forward(float distance) {
    eye_point = eye_point - distance * back_vector;

    return *this;
}

Camera& Camera::yaw(float angle) {
    Affine R_theta_v = rotate(angle, up_vector);

    right_vector = R_theta_v * right_vector;
    back_vector = R_theta_v * back_vector;

    return *this;
}

Camera& Camera::pitch(float angle) {
    Affine R_theta_u = rotate(angle, right_vector);

    up_vector = R_theta_u * up_vector;
    back_vector = R_theta_u * back_vector;

    return *this;
}

Camera& Camera::roll(float angle) {
    Affine R_theta_n = rotate(angle, back_vector);

    right_vector = R_theta_n * right_vector;
    up_vector = R_theta_n * up_vector;

    return *this;
}

