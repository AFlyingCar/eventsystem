/**
 * @file    CubeMesh.cpp
 * @date    2018-01-23
 *
 * @author  Tyler Robbins
 * 
 * @par Term:       Spring
 * @par Course:     CS250-A
 * @par Assignment: #02
 */

#include "CubeMesh.h"

const Point       CubeMesh::vertices[8] = { Point( 1,  1,  1),
                                            Point( 1,  1, -1),
                                            Point( 1, -1,  1),
                                            Point( 1, -1, -1),
                                            Point(-1,  1,  1),
                                            Point(-1,  1, -1),
                                            Point(-1, -1,  1),
                                            Point(-1, -1, -1)
                                          };

const Mesh::Face CubeMesh::faces[12] = { {6,2,0}, {6,0,4}, // Front
                                         {2,3,1}, {2,1,0}, // Right
                                         {3,7,1}, {7,5,1}, // Back
                                         {7,6,5}, {6,4,5}, // Left
                                         {4,0,1}, {4,1,5}, // Top
                                         {6,7,3}, {6,3,2}  // Bottom
                                       };

const Mesh::Edge CubeMesh::edges[12] = { {0,1}, {0,2}, {0,4}, {1,3},
                                         {1,5}, {2,3}, {2,6}, {3,7},
                                         {4,5}, {4,6}, {5,7}, {6,7}
                                       };

int CubeMesh::vertexCount() {
    return 8;
}

Point CubeMesh::getVertex(int i) {
    return CubeMesh::vertices[i];
}

Vector CubeMesh::dimensions() {
    return Vector(2, 2, 2);
}

Point CubeMesh::center() {
    return Point();
}

int CubeMesh::faceCount() {
    return 12;
}

Mesh::Face CubeMesh::getFace(int i) {
    return faces[i];
}

int CubeMesh::edgeCount() {
    return 12;
}

Mesh::Edge CubeMesh::getEdge(int i) {
    return edges[i];
}
