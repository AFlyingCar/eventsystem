/**
 * @file    FrustumMesh.cpp
 * @date    2018-02-01
 *
 * @author  Tyler Robbins
 *
 * @par Term:       Spring
 * @par Course:     CS250-A
 * @par Assignment: #04
 */

#include "FrustumMesh.h"

const Mesh::Edge FrustumMesh::edges[16] = {
    Mesh::Edge(0, 1),
    Mesh::Edge(0, 2),
    Mesh::Edge(0, 3),
    Mesh::Edge(0, 4),
    Mesh::Edge(1, 2),
    Mesh::Edge(2, 3),
    Mesh::Edge(3, 4),
    Mesh::Edge(4, 1),
    Mesh::Edge(1, 5),
    Mesh::Edge(2, 6),
    Mesh::Edge(3, 7),
    Mesh::Edge(4, 8),
    Mesh::Edge(5, 6),
    Mesh::Edge(6, 7),
    Mesh::Edge(7, 8),
    Mesh::Edge(8, 5)
};

const Mesh::Face FrustumMesh::faces[12] = {
    Mesh::Face(1, 2, 6),
    Mesh::Face(1, 6, 5),
    Mesh::Face(2, 7, 6),
    Mesh::Face(2, 3, 7),
    Mesh::Face(4, 8, 7),
    Mesh::Face(4, 7, 3),
    Mesh::Face(1, 5, 8),
    Mesh::Face(1, 8, 4),
    Mesh::Face(1, 3, 2),
    Mesh::Face(1, 4, 3),
    Mesh::Face(5, 6, 7),
    Mesh::Face(5, 7, 8)
};

/*

3D ASCII diagrams are hard... :(

6-----------------------7
| \                  __-|
|  \             __--   |
|    2---------3        |
|    |         |        |
|    |    0    |        |
|__--1---------4---_____|
5-----------------------8


 */

FrustumMesh::FrustumMesh(float fov, float aspect, float N, float F):
    vertices(),
    center_point(0, 0, 0),
    viewport_dimensions()
{
    // Calculate Near face dimensions
    float Wn = 2.0f * N * std::tan(fov/2.0f);
    float Hn = Wn/aspect;

    // Calculate far face dimensions
    float Wf = 2 * F * std::tan(fov/2);
    float Hf = Wf/aspect;

    viewport_dimensions = Vector(Wn, Hn);

    // Cache these
    float half_wn = Wn/2.0f;
    float half_hn = Hn/2.0f;

    float half_wf = Wf/2.0f;
    float half_hf = Hf/2.0f;

    // :(
    vertices[0] = Point(0, 0, 0);

    vertices[1] = Point(-half_wn, -half_hn, -N);
    vertices[2] = Point(-half_wn,  half_hn, -N);
    vertices[3] = Point( half_wn,  half_hn, -N);
    vertices[4] = Point( half_wn, -half_hn, -N);

    vertices[5] = Point(-half_wf, -half_hf, -F);
    vertices[6] = Point(-half_wf,  half_hf, -F);
    vertices[7] = Point( half_wf,  half_hf, -F);
    vertices[8] = Point( half_wf, -half_hf, -F);
}

int FrustumMesh::vertexCount() {
    return 9;
}

Point FrustumMesh::getVertex(int i) {
    return vertices[i];
}

Vector FrustumMesh::dimensions(void) {
    return viewport_dimensions;
}

Point FrustumMesh::center(void) {
    return center_point;
}

int FrustumMesh::faceCount(void) {
    return 12;
}

Mesh::Face FrustumMesh::getFace(int i) {
    return faces[i];
}

int FrustumMesh::edgeCount(void) {
    return 16;
}

Mesh::Edge FrustumMesh::getEdge(int i) {
    return edges[i];
}

