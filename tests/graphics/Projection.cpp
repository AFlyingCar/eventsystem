/**
 * @file    Projection.cpp
 * @date    2018-02-15
 *
 * @author  Tyler Robbins
 * 
 * @par Term:       Fall
 * @par Course:     CS250-A
 * @par Assignment: #05
 */

#include "Projection.h"

Affine cameraToWorld(const Camera& cam) {
    return Affine(cam.right(), cam.up(), cam.back(), cam.eye());
}

Affine worldToCamera(const Camera& cam) {
    return inverse(cameraToWorld(cam));
}

Matrix cameraToNDC(const Camera& cam) {
    // W, H, D
    Vector dimensions = cam.viewportGeometry();

    // Cache this
    const float w = dimensions[0];
    const float h = dimensions[1];
    const float d = dimensions[2];
    const float n = cam.nearDistance();
    const float f = cam.farDistance();

    float n_minus_f = n - f;

    Matrix m;
    m[0] = Hcoord((2 * d) / w);
    m[1] = Hcoord(0, (2 * d) / h);
    m[2] = Hcoord(0, 0, (n + f) / n_minus_f, (2 * n * f) / n_minus_f);
    m[3] = Hcoord(0, 0, -1, 0);

    return m;
}

