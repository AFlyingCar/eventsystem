#ifndef CLIENT_H
#define CLIENT_H

#include <sstream>
#include <iostream> // std::cout, std::endl
#include <ctime>
#include <vector> // std::vector
#include <random> // std::uniform_int_distribution, std::random_device, std::mt19937
#include <bitset> // std::bitset

#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <GL/gl.h>

#include "EventSubscribe.h"
#include "EventHandler.h"
#include "EventBase.h"

#include "Affine.h"        // Point, Vector
#include "Mesh.h"          // Mesh
#include "CubeMesh.h"      // CubeMesh
#include "SnubDodecMesh.h" // SnubNoseDodec
#include "Camera.h"        // Camera
#include "Projection.h"    // worldToCamera, cameraToNDC
#include "Render.h"        // Render

struct QuitEvent: public EventSystem3::EventBase {
    PARENT(EventBase);
};

struct KeyPressEvent: public EventSystem3::EventBase {
    PARENT(EventBase);

    KeyPressEvent(SDL_Keycode kc): keycode(kc) { }

    SDL_Keycode keycode;
};

struct WindowResizeEvent: public EventSystem3::EventBase {
    PARENT(EventBase);

    WindowResizeEvent(int w, int h): W(w), H(h) { }

    int W;
    int H;
};

struct DrawEvent: public EventSystem3::EventBase {
    PARENT(EventBase);

    DrawEvent(double dt): dt(dt) { }

    double dt;
};

namespace {
    const Point O(0, 0, 0); //!< The origin

    const Vector EX(1, 0, 0); //!< The X axis
    const Vector EY(0, 1, 0); //!< The Y axis
    const Vector EZ(0, 0, 1); //!< The Z axis

    const Point E(0, 0, 0); //!< The eye point

    const Vector L(0, 0, 1); //!< Light direction

    const float ABS_L = abs(L); //!< Magnitude of the light direction
    const float PI = 4.0f * atan(1.0f); //!< PI
    const int ABS_POS_MAX = 5;
    const float ERF_DIST = 2; //!< Distance of erf from the first camera
    const float MUN_DIST = 2; //!< Distance of mun from the first camera
    const float MOV_DIST = 2; //!< Distance of mov from the first camera
    const float MOV_MAX_MOV_DIST = 2.0f; //!< Maximum distance mov can move

    const float CAM_FOV = 0.5 * PI; //!< FOV of the camera

    const float ERF_ROT_RATE = (2 * PI) / 10.0f; //!< rotation rate of erf
    const float MUN_ROT_RATE = PI / 3.0f; //!< rotation rate of mun
    const float MOV_ROT_RATE = PI / 6.0f; //!< rotation rate of mov
    const float MOV_MOVE_RATE = 1; //!< rate of motion of mov

    const float MUN_ORBIT_RADIUS = 2; //!< radius of mun's orbit

    const int UP_CUBES = 5; //!< number of up-specifying cubes
    const float UP_CUBE_START_X = -3; //!< Starting X of the up-specifying cubes
    const float UP_CUBE_Y = 3; //!< Y value of the up-specifying cubes
    const float UP_CUBE_Z = -3; //!< Z value of the up-specifying cubes
    const float UP_CUBE_SEP = 0.1f; //!< distance between each up-specifying cube
    const float UP_CUBE_SCALE = 0.4; //!< scale of the up-specifying cubes

    const Vector ERF_ROT_AXIS = EY; //!< rotation axis of erf
    const Vector MUN_ROT_AXIS = EY; //!< rotation axis of mun

    const Point ERF_CENTER(0, 0, -ERF_DIST); //!< center of erf
    const Point MUN_CENTER(0, 0, -MUN_DIST); //!< center of mun
    const Point MOV_CENTER(4, 0, -MOV_DIST); //!< cetner of mov

    const float MAX_MOV_POS = 4; //!< maximum position mov can be at

    const Vector WHITE(1, 1, 1); //!< the color white
    const Vector PURPLE(1, 0, 1); //!< the color purple
    const Vector BLACK(0, 0, 0); //!< the color black
    const Vector RED(1, 0, 0); //!< the color red
    const Vector BLUE(0, 0, 1); //!< the color blue
    const Vector GREEN(0, 1, 0); //!< the color green
    const Vector AA9F39(0.667, 0.624, 0.224); //!< the color AA9F39

    const std::bitset<2> ZERO(0); //!< zero represetned with 2 bits
    const std::bitset<2> ONE(1); //!< one represented with 2 bits

}

/**
 * @brief An object.
 */
struct Object {
    Mesh* mesh; //!< The mesh
    Affine model; //!< the model transformation
    Vector color; //!< the color
};

class Client: public EventSystem3::EventHandler {
    public:
        Client(SDL_Window* w): EventSystem3::EventHandler(this),
                               m_window(w),
                               m_frame_count(0),
                               m_frame_time(0),
                               m_total_frame_time(0),
                               m_mov_theta(PI/2.0f),
                               m_cam1(O + EZ, -EZ, EY, 0.5f * PI, 1, 0.01f,
                                      100),
                               m_cam2(MOV_CENTER, -EX, EY, 0.5f*PI, 1,
                                      0.01f, 100),
                               m_cam3(MOV_CENTER, EX, EY, 0.5f*PI, 1,
                                      0.01f, 100),
                               m_selected_cam(0x1),
                               m_objects(),
                               m_erf(),
                               m_mun(),
                               m_mov(),
                               m_render(),
                               m_cube(),
                               m_snub(),
                               m_random(),
                               m_gen(m_random()),
                               m_obj_dist(100, 200),
                               m_pos_dist(-ABS_POS_MAX, ABS_POS_MAX),
                               m_rot_dist(0, PI / 2.0f),
                               m_scale_dist(0.1, 0.3),
                               m_quit(false)
        {
            eh_init();
            // generate all of our objects
            int obj_count = m_obj_dist(m_gen);
            for(int i = 0; i < obj_count; ++i) {
                float x = static_cast<float>(m_pos_dist(m_gen));
                float y = static_cast<float>(m_pos_dist(m_gen));
                float z = static_cast<float>(m_pos_dist(m_gen));

                // Make sure none of our objects are in the way of the camera
                if(inWayOfCam1(x,y,z)) {
                    --i;
                    continue;
                }

                // Because why generate a random axis to rotate around, when we can just
                //  rotate around all 3 unit axes by random amounts.
                float thetax = static_cast<float>(m_rot_dist(m_gen));
                float thetay = static_cast<float>(m_rot_dist(m_gen));
                float thetaz = static_cast<float>(m_rot_dist(m_gen));

                // They are all cubes
                m_objects.push_back({&m_cube,
                                     translate(Vector(x, y, z)) *
                                     rotate(thetax, EX) *
                                     rotate(thetay, EY) *
                                     rotate(thetaz, EZ) *
                                     scale(m_scale_dist(m_gen)),
                                     PURPLE});
            }
            std::cout << "Generated " << obj_count << " static objects at random "
                      << "positions, rotations, and scalings." << std::endl;

            // Build the up-specifying objects so we know which way is up

            float x = UP_CUBE_START_X;

            for(int i = 0; i < UP_CUBES; ++i) {
                x += (m_cube.dimensions().x / 2.0f) + UP_CUBE_SEP;

                m_objects.push_back({&m_cube,
                                     translate(Vector(x, UP_CUBE_Y, UP_CUBE_Z)) *
                                     scale(UP_CUBE_SCALE),
                                     AA9F39});
            }

            std::cout << "Generated " << UP_CUBES << " up-specifying cubes."
                      << std::endl;

            // Generate all moving objects

            m_erf.mesh = &m_snub;
            m_erf.model = translate(ERF_CENTER - O);
            m_erf.color = GREEN;

            m_mun.mesh = &m_snub;
            m_mun.model = translate(MUN_CENTER - O) *
                          translate(Vector(MUN_ORBIT_RADIUS)) * scale(0.1);
            m_mun.color = BLUE;

            m_mov.mesh = &m_cube;
            m_mov.model = translate(MOV_CENTER - O) * scale(0.2);
            m_mov.color = RED;

            std::cout << "Generated " << 3 << " non-static objects." << std::endl;

            std::cout << "Note: If the static cubes are in the way of the camera, try "
                      << "re-running the program to get a better distribution of cubes."
                      << std::endl;

        }

        void HandleQuit(QuitEvent&) {
            std::cout << "Quitting now!" << std::endl;
            m_quit = true;
        }
        INCLASS SUBSCRIBE(QuitEvent, &Client::HandleQuit, EHCLASS(Client),);

        void HandleKeypress(KeyPressEvent& kp) {
            // respond to keyboard input
            //   kc: SDL keycode (e.g., SDLK_SPACE, SDLK_a, SDLK_s)
            if(kp.keycode == SDLK_SPACE) cycleCamera();
        }
        INCLASS SUBSCRIBE(KeyPressEvent, &Client::HandleKeypress, EHCLASS(Client),);


        void HandleResize(WindowResizeEvent& wre) {
            // respond to window resize
            //   W,H: window width and height (in pixels)
            // int D = std::min(wre.W, wre.H);
            glViewport(0, 0, wre.W, wre.H);

            // :(
            m_cam1 = Camera(m_cam1.eye(), -m_cam1.back(), m_cam1.up(), CAM_FOV,
                            static_cast<float>(wre.W)/wre.H, m_cam1.nearDistance(),
                            m_cam1.farDistance());
            m_cam2 = Camera(m_cam2.eye(), -m_cam2.back(), m_cam2.up(), CAM_FOV,
                            static_cast<float>(wre.W)/wre.H, m_cam2.nearDistance(),
                            m_cam2.farDistance());
            m_cam3 = Camera(m_cam3.eye(), -m_cam3.back(), m_cam3.up(), CAM_FOV,
                            static_cast<float>(wre.W)/wre.H, m_cam3.nearDistance(),
                            m_cam3.farDistance());
        }
        INCLASS SUBSCRIBE(WindowResizeEvent, &Client::HandleResize, EHCLASS(Client),);

        void HandleDraw(DrawEvent& de) {
            //   dt: time (in seconds) since last animation frame
            // frame rate:
            ++m_frame_count;
            m_frame_time += de.dt;
            m_total_frame_time += de.dt;
            if (m_frame_time >= 0.5) {
                double fps = m_frame_count/m_frame_time;
                m_frame_count = 0;
                m_frame_time = 0;
                std::stringstream ss;
                ss << "CS 250: Project #1 [fps=" << int(fps) << "]";
                SDL_SetWindowTitle(m_window,ss.str().c_str());
            }

            // Update positions of all moving objects
            updatePositions(de.dt);

            // drawing code:
            Matrix camera2NDC = cameraToNDC(getCamera());
            Matrix world2camera = worldToCamera(getCamera());

            m_render.clearBuffers(WHITE);
            for(Object& o : m_objects) {
                render(o, world2camera, camera2NDC);
            }

            render(m_erf, world2camera, camera2NDC);
            render(m_mun, world2camera, camera2NDC);
            render(m_mov, world2camera, camera2NDC);

            SDL_GL_SwapWindow(m_window);
        }
        INCLASS SUBSCRIBE(DrawEvent, &Client::HandleDraw, EHCLASS(Client),);

        bool DoQuit() const {
            return m_quit;
        }

    protected:
        const Camera& getCamera() const{
            // This was pretty fun to write
            if(m_selected_cam.test(0))
                if(m_selected_cam.test(1))
                    return m_cam3;
                else
                    return m_cam1;
            else if(m_selected_cam.test(1))
                return m_cam2;
            else
                throw std::runtime_error("INVALID CAMERA SET!"); // We cannot have 4 be
                                                                 //  a camera to swtich
                                                                 //  to, as there are
                                                                 //  only 3 of them.
        }
        void cycleCamera(){
            // Woo! Creating a bit-wise adder in C++!
            // Because why not!
            if(m_selected_cam.test(0) && m_selected_cam.test(1)) {
                m_selected_cam = ONE;

                return;
            } else {
                if((m_selected_cam[0] = m_selected_cam[0] ^ true) == true) return;
                m_selected_cam[1] = m_selected_cam[1] ^ true;
            }
        }

        void render(const Object& object, const Matrix& w2c,
                    const Matrix& c2ndc)
        {
            Mesh* mesh = object.mesh;

            Affine obj2cam = w2c * object.model;

            // Cache transformed vertices
            m_transformed.clear();
            for(int i = 0; i < mesh->vertexCount(); ++i) {
                m_transformed.push_back(obj2cam * mesh->getVertex(i));
            }

            // Draw faces
            for(int i = 0; i < mesh->faceCount(); ++i) {
                Mesh::Face f = mesh->getFace(i);

                Hcoord P = m_transformed.at(f.index1);
                Hcoord Q = m_transformed.at(f.index2);
                Hcoord R = m_transformed.at(f.index3);

                Vector m = cross(Q - P, R - P);

                // If it is not visible, then skip this face
                if(dot(m, E - P) <= 0 && dot(m, E - Q) <= 0 && dot(m, E - R) <= 0)
                    continue;

                P = c2ndc * P;
                Q = c2ndc * Q;
                R = c2ndc * R;

                // LET THERE BE LIGHT!
                float u = std::abs(dot(L, m)) / static_cast<float>(ABS_L * abs(m));
                Vector ucolor = u * object.color;

                m_render.setColor(ucolor);

                m_render.fillTriangle(P, Q, R);
            }
        }

        void updatePositions(double dt) {
            // ERF
            // ===
            m_erf.model = translate(ERF_CENTER - O) *
                          rotate(static_cast<float>(dt * ERF_ROT_RATE), ERF_ROT_AXIS) *
                          translate(O - ERF_CENTER) * m_erf.model;

            // MUN
            // ===
            m_mun.model = translate(MUN_CENTER - O) * rotate(dt, MUN_ROT_AXIS) *
                          rotate(dt * MUN_ROT_RATE, EZ) * translate(O - MUN_CENTER) *
                          m_mun.model;

            // MOV
            // ===
            m_mov_theta = m_total_frame_time;
            float sin_theta = std::sin(m_mov_theta);
            float cos_theta = std::cos(PI + m_mov_theta);
            Point newMovCenter(MOV_CENTER);
            newMovCenter.x = MOV_CENTER.x - (MOV_MAX_MOV_DIST * sin_theta);


            // Rotate by dt * MOV_ROT_RATE around the X axis
            m_mov.model = translate(newMovCenter - O) * rotate(dt * MOV_ROT_RATE, EX) *
                          translate(O - m_cam2.eye()) * m_mov.model;

            // CAM 2
            // =====
            float dist = m_cam2.eye().x - newMovCenter.x;

            m_cam2.forward(dist);

            // CAM 3
            // =====
            // AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
            //   OH GOD WHY!!!!!!!!!!!!!!!!!!
            m_cam3.forward(dist * (cos_theta > 0 ? -1 : 1));
            m_cam3.yaw(dot(-m_cam3.back(), Vector(cos_theta)) < 0 ? PI : 0);
            m_cam3.roll(dt * MOV_ROT_RATE);
        }

        const Vector& getRandAxis();

        bool inWayOfCam1(float x, float y, float z) {
            Vector geom = m_cam1.viewportGeometry();
            Point eye = m_cam1.eye();

            // :)
            return (x > (eye.x - geom.x) && x < (eye.x + geom.x)) ||
                   (y > (eye.y - geom.y) && y < (eye.y + geom.y)) ||
                   (z > (eye.z + m_cam1.nearDistance()) && z < ERF_DIST);
        }
    private:
        SDL_Window* m_window;
        int m_frame_count;
        double m_frame_time;
        // other variables:
        double m_total_frame_time; //!< Total time passed

        float m_mov_theta; //!< Mov's angle

        Camera m_cam1; //!< Cam 1
        Camera m_cam2; //!< Cam 2
        Camera m_cam3; //!< cam 3

        std::bitset<2> m_selected_cam; //!< 2 bit uint of the selected camera

        std::vector<Object> m_objects; //!< All static objects

        Object m_erf; //!< erf
        Object m_mun; //!< mun
        Object m_mov; //!< mov

        Render m_render; //!< The renderer

        CubeMesh m_cube; //!< Cubes
        SnubDodecMesh m_snub; //!< Not-spheres

        std::random_device m_random; //!< Randomness
        std::mt19937 m_gen; //!< random number generator [magic]
        std::uniform_int_distribution<> m_obj_dist; //!< object distribution
        std::uniform_real_distribution<> m_pos_dist; //!< position distribution
        std::uniform_real_distribution<> m_rot_dist; //!< rotation distribution
        std::uniform_real_distribution<> m_scale_dist; //!< scale distribution

        std::vector<Point> m_transformed; //!< temp vector for transformed points
        bool m_quit;
};

#endif

