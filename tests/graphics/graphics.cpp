#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <GL/gl.h>

#include "EventBus.h"
#include "Client.h"

int main(int, char**) {
    srand(unsigned(time(0)));

    // SDL: initialize and create a window
    SDL_Init(SDL_INIT_VIDEO);
    const char *title = "CS250: Project #1";
    int width  = 600;
    int height = 600;
    SDL_Window *window = SDL_CreateWindow(title,SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,width,height,
            SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE);
    SDL_GLContext context = SDL_GL_CreateContext(window);

    // GLEW: get function bindings (if possible)
    GLenum value = glewInit();
    if (value != GLEW_OK) {
        std::cout << glewGetErrorString(value) << std::endl;
        SDL_GL_DeleteContext(context);
        SDL_Quit();
        return -1;
    }

    // animation loop
    try {
        Client *client = new Client(window);
        Uint32 ticks_last = SDL_GetTicks();
        while (!client->DoQuit()) {
            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                switch (event.type) {
                    case SDL_QUIT:
                        EVENT_BUS_FIRE(new QuitEvent(),,);
                        break;
                    case SDL_KEYDOWN:
                        if (event.key.keysym.sym == SDLK_ESCAPE)
                            EVENT_BUS_FIRE(new QuitEvent(),,);
                        else
                            EVENT_BUS_FIRE(new KeyPressEvent(event.key.keysym.sym),,);
                        break;
                    case SDL_WINDOWEVENT:
                        if (event.window.event == SDL_WINDOWEVENT_RESIZED)
                            EVENT_BUS_FIRE(new WindowResizeEvent(event.window.data1,
                                                                 event.window.data2),,);
                        break;
                }
            }

            Uint32 ticks = SDL_GetTicks();
            double dt = 0.001*(ticks - ticks_last);
            ticks_last = ticks;

            EVENT_BUS_FIRE(new DrawEvent(dt),,);

            EVENT_BUS.update(dt);
        }

        delete client;
    } catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    SDL_GL_DeleteContext(context);
    SDL_Quit();
    return 0;
}

