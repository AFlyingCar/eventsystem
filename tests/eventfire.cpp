#include <iostream>

#include "EventBus.h"
#include "EventBase.h"

struct MyEvent: public EventSystem3::EventBase {
    PARENT(EventBase);
};

int main(int, char**) {
    EVENT_BUS.fire<MyEvent>();

    std::cout << EVENT_BUS.getEventQueueSize() << std::endl;

    return 0;
}

