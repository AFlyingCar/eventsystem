
#include <iostream>

#include "EventBus.h"
#include "EventBase.h"
#include "EventSubscribe.h"
#include "EventHandler.h"

struct MyEvent: public EventSystem3::EventBase {
    PARENT(EventBase);
};

struct MyHandler: public EventSystem3::EventHandler {
    MyHandler(): EventSystem3::EventHandler(this) {
        SUBSCRIBE(MyEvent, &MyHandler::handler2, this,);
        eh_init();
    }

    void handler(MyEvent&) {
        std::cout << "Handling from handler in " << this << std::endl;
    }
    INCLASS SUBSCRIBE(MyEvent, &MyHandler::handler, EHCLASS(MyHandler),);

    void handler2(MyEvent&) {
        std::cout << "Handling from handler2 in " << this << std::endl;
    }
};

int main(int, char**)
{
    std::cout << "================================" << std::endl;
    std::cout << "=== Firing MyEvent 10 times. ===" << std::endl;
    std::cout << "================================" << std::endl;

    for(int i = 0; i < 10; ++i) {
        EVENT_BUS.fire<MyEvent>();
        EVENT_BUS.update(0.0f);
    }

    std::cout << "======================================" << std::endl;
    std::cout << "=== Constructing MyHandler Object. ===" << std::endl;
    std::cout << "======================================" << std::endl;

    {
        MyHandler mh;
        for(int i = 0; i < 10; ++i) {
            EVENT_BUS.fire<MyEvent>();
            EVENT_BUS.update(0.0f);
        }
    }

    std::cout << "================================" << std::endl;
    std::cout << "=== Firing MyEvent 10 times. ===" << std::endl;
    std::cout << "================================" << std::endl;

    for(int i = 0; i < 10; ++i) {
        EVENT_BUS.fire<MyEvent>();
        EVENT_BUS.update(0.0f);
    }


}


