#include <iostream>

#include "EventBus.h"

int main(int,char**) {

    for(int i = 0; i < 100; ++i) {
        std::cout << "Update #" << i << std::endl;
        EventSystem3::EventBus::getInstance().update(0.0f);
    }
    return 0;
}

